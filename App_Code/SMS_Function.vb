﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization

Imports System.Xml.Schema
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class SMS_Function

    Dim MyOMGFunction As New OMG_Function

    Function SendSMS62100(ByVal MyRequest As INT_SendSMSReq, ByVal RecordID As String, Optional ByVal OrderNumber As String = "", Optional ByVal IVCode As String = "", Optional ByVal VCode As String = "") As GeneralResponse

        Dim myResponse As New GeneralResponse

        'declare the web service path and parameter
        Dim SMSWebService As New SMS.SMSWebService
        Dim myMerchantSendSMSRequest As New SMS.MerchantSendSMSRequest
        Dim myMerchantSendSMSResponse As New SMS.MerchantSendSMSResponse

        If (Trim(MyRequest.MobileNumber) <> "") And (MyRequest.MobileNumber <> "NONE") And (Not MyRequest.MobileNumber Is Nothing) And (MyRequest.MobileNumber.Length <> 0) Then

            ' Generate Transaction ID
            If Trim(RecordID) = "" Then
                Dim ModTime As String = ""
                ModTime = DateTime.Now.ToString("yyyyMMdd")
                ModTime = ModTime & TimeOfDay.TimeOfDay.ToString("")
                ModTime = Replace(ModTime, " ", "")
                ModTime = Replace(ModTime, ":", "")
                RecordID = ModTime
            End If

            'assign value
            Dim charstotrim() As Char = {" "c}
            myMerchantSendSMSRequest.Merchant = "OMG"
            myMerchantSendSMSRequest.MobileNum = MyRequest.MobileNumber.Trim(charstotrim)
            myMerchantSendSMSRequest.RequestID = RecordID
            myMerchantSendSMSRequest.SMSMessage = MyRequest.StrMsg
            Dim SMSMerchantKey = "St4rb&k@76"

            ' create signature
            Dim MySignature As String
            MySignature = "##" + myMerchantSendSMSRequest.Merchant + "&&" + SMSMerchantKey + "&&" + myMerchantSendSMSRequest.RequestID + "##"
            MySignature = MyOMGFunction.GetEncryption(MySignature, True)

            myMerchantSendSMSRequest.Signature = MySignature

            myMerchantSendSMSResponse = SMSWebService.MerchantSendOutboundSMS(myMerchantSendSMSRequest)

            myResponse.ResponseCode = myMerchantSendSMSResponse.ResponseCode
            myResponse.ResponseMsg = myMerchantSendSMSResponse.ResponseMsg
        Else
            myResponse.ResponseCode = 101
            myResponse.ResponseMsg = "Mobile number is null"
        End If

        Dim MyDb As New dbConn
        Dim strConn As New SqlConnection(MyDb.connString)

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim cmd As New SqlCommand("SendSMSLog", strConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@TMAccountID", SqlDbType.VarChar, 50).Value = MyRequest.TM_AccountID
            cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 50).Value = MyRequest.ProductId

            If (MyRequest.MobileNumber.Length <> 0) Then
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar, 20).Value = MyRequest.MobileNumber
            End If

            If Trim(OrderNumber) <> "" Then
                cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar, 50).Value = OrderNumber
            End If

            If Trim(IVCode) <> "" And Trim(VCode) <> "" And myResponse.ResponseCode <> 0 Then
                cmd.Parameters.Add("@InitialVCode", SqlDbType.VarChar, 4).Value = IVCode
                cmd.Parameters.Add("@VCode", SqlDbType.VarChar, 6).Value = VCode
            End If

            If myResponse.ResponseCode <> "101" Then
                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Value = myResponse.ResponseCode
                cmd.Parameters.Add("@MessageId", SqlDbType.VarChar, 50).Value = myResponse.ResponseMsg
                cmd.Parameters.Add("@SessionID", SqlDbType.VarChar, 50).Value = "NA"
                cmd.Parameters.Add("@SMSTransactionID", SqlDbType.VarChar, 50).Value = RecordID
                cmd.Parameters.Add("@SMSMsg", SqlDbType.VarChar, -1).Value = MyRequest.StrMsg
            End If

            cmd.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = myResponse.ResponseMsg & " (" & myResponse.ResponseCode & ")"

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            myResponse.ResponseCode = 2
            myResponse.ResponseMsg = ex.ToString
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        Return myResponse

    End Function

End Class
