﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System

Public Class CDR_Function

    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)
    Dim FTPFunction As New File_Function
    Dim OMGFunction As New OMG_Function

    Function ProcessCDR() As OMG_ValidateResp

        Dim ProcessCDRResp As New OMG_ValidateResp

        Dim FileDirectory As String = "E:\www\OMG_prod\CDR"
        'Dim FileDirectory As String = "E:\www\OMG_dev\CDR"
        Dim ProcessDataInLine As Boolean = False
        Dim ProcessCurrentFile As Boolean = False

        'Dim TotalFile As Integer = 0
        'Dim FileArray As New List(Of String)
        'Dim TempFile As String = ""

        Dim fileEntries As String() = Directory.GetFiles(FileDirectory)
        Dim fileName As String

        For Each fileName In fileEntries

            If Path.GetExtension(fileName) = ".dat" Then

                Dim TotalLine As Integer = 0
                Dim TotalRowInsert As Integer = 0
                Dim TotalRecord As Integer = 0

                Using r As StreamReader = New StreamReader(fileName)

                    Dim data As String
                    data = r.ReadLine

                    Try
                        If strConn.State = ConnectionState.Closed Then
                            strConn.Open()
                        End If

                        Do While (Not data Is Nothing)

                            TotalLine = TotalLine + 1

                            Dim dataArr() As String
                            dataArr = data.Split("|")

                            If dataArr.Length = 21 Then

                                Select Case dataArr(10)
                                    Case "593", "INT25086", "408", "INT24269", "596", "INT25088", "595", "INT25087", "398", _
                                            "INT24266", "640", "INT25133", "406", "INT24267", "503", "INT24325", "664"
                                        ProcessDataInLine = True
                                    Case Else
                                        'Do nothing
                                End Select

                                If ProcessDataInLine = True Then

                                    Dim tempDate As String = dataArr(4).Substring(0, 4) & "-" & dataArr(4).Substring(4, 2) & _
                                                    "-" & dataArr(4).Substring(6, 2) & " " & _
                                                    dataArr(4).Substring(8, 2) & ":" & dataArr(4).Substring(10, 2) & _
                                                    ":" & dataArr(4).Substring(12, 2)

                                    Dim SubsDate As DateTime = DateTime.ParseExact(tempDate, "yyyy-MM-dd HH:mm:ss", Nothing)
                                    'Dim CycleDate As Integer = SubsDate.Day

                                    Dim cmd As New SqlCommand
                                    cmd.CommandType = CommandType.Text
                                    cmd.Connection = strConn
                                    cmd.CommandText = _
                                                "INSERT INTO temp_cdr_tbl (subscriberID,IPTV_id,accountNo,tenantID,productID," & _
                                                "contentType,subscription_date,cdr_filename) " & _
                                                "VALUES (@subscriberID,@iptvID,@accountNo,@tenantID,@productID," & _
                                                "@contentType,@subscriptionDate,@cdrFilename) " & _
                                                "SELECT SCOPE_IDENTITY();"

                                    cmd.Parameters.AddWithValue("@subscriberID", dataArr(1))
                                    cmd.Parameters.AddWithValue("@iptvID", dataArr(1))
                                    cmd.Parameters.AddWithValue("@accountNo", dataArr(2))
                                    cmd.Parameters.AddWithValue("@tenantID", dataArr(18))
                                    cmd.Parameters.AddWithValue("@productID", dataArr(10))
                                    cmd.Parameters.AddWithValue("@contentType", dataArr(9))
                                    cmd.Parameters.AddWithValue("@subscriptionDate", SubsDate.ToString("yyyy-MM-dd HH:mm:ss"))
                                    cmd.Parameters.AddWithValue("@cdrFilename", fileName.Replace("E:\www\OMG_prod\CDR\", ""))
                                    'cmd.Parameters.AddWithValue("@cdrFilename", fileName.Replace("E:\www\OMG_dev\CDR\", ""))

                                    Dim identity As Integer = -1
                                    identity = Integer.Parse(cmd.ExecuteScalar().ToString())

                                    If identity <> -1 Then
                                        TotalRowInsert = TotalRowInsert + 1
                                    Else
                                        'Insert into ErrorCDRTransaction_tbl
                                        'Error insert into temp_cdr_tbl
                                    End If

                                End If
                            Else
                                If data.Contains("Record Total") Then
                                    dataArr = data.Split(":")
                                    TotalRecord = dataArr(1)

                                ElseIf data.Contains("|") Then
                                    'Insert into ErrorCDRTransaction_tbl
                                    'Not enough data in a line
                                End If
                            End If

                            ProcessDataInLine = False
                            data = r.ReadLine
                        Loop

                        If (TotalLine - 1) = TotalRecord Then
                            ProcessCurrentFile = True
                        End If

                    Catch ex As Exception
                        ProcessCDRResp.ResponseCode = 101
                        ProcessCDRResp.ResponseMsg = "Technical error - For file: " & fileName
                        'ProcessCDRResp.ResponseMsg = ex.ToString
                    Finally
                        If strConn.State = ConnectionState.Open Then
                            strConn.Close()
                        End If
                    End Try
                End Using

                If ProcessCurrentFile = True Then

                    Dim TempDate As Date = Date.Now()
                    Dim todayFile As String = TempDate.ToString("dd_MMM_yyyy")

                    Dim NewFileName As String = fileName.Replace("E:\www\OMG_prod\CDR\", "E:\www\OMG_prod\CDR\PROCESS\" & todayFile & "\")
                    'Dim NewFileName As String = fileName.Replace("E:\www\OMG_dev\CDR\", "E:\www\OMG_dev\CDR\PROCESS\" & todayFile & "\")

                    If Directory.Exists(FileDirectory & "\PROCESS\" & todayFile) Then
                        'Do nothing
                    Else
                        Directory.CreateDirectory(FileDirectory & "\PROCESS\" & todayFile)
                    End If

                    'Move the file
                    If File.Exists(NewFileName) Then
                        File.Delete(NewFileName)
                    End If
                    File.Move(fileName, NewFileName)

                    ProcessCDRResp.ResponseCode = 0
                    ProcessCDRResp.ResponseMsg = "Success"
                Else
                    'Ínsert into Exception_tbl
                    'ProcessCDRResp.ResponseCode = TotalRecord
                    'ProcessCDRResp.ResponseMsg = "Not enough data process: " & TotalLine & ", " & TotalRowInsert & ", " & TotalRecord
                End If

            End If

            ProcessCurrentFile = False
        Next fileName

        Return ProcessCDRResp
    End Function

    Function GetEncryption(ByVal toHash As String) As String
        Dim buffer As Byte() = Encoding.Default.GetBytes(toHash)
        Dim cryptoTransformSha1 As New SHA1CryptoServiceProvider()
        Dim hash As String = BitConverter.ToString(cryptoTransformSha1.ComputeHash(buffer)).Replace("-", "")
        Return hash
    End Function

End Class
