﻿<%@ WebService Language="VB" Class="TMOMGSystem" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports System.Web.Script.Services
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://omg.hypp.tv/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class TMOMGSystem
    Inherits System.Web.Services.WebService

    Dim MyOMGFunction As New OMG_Function
    Dim MyTMFunction As New TMBilling_Function
    Dim MyMerchant As New MerchantDetails
    Dim b4hashstring As String
    Dim MySignature As String
    Dim APILog As New INT_APILog
    Dim ErrorMessage As String = ""

    <WebMethod(Description:="OTT activation order from SDP.")>
    Public Function OMG_OrderRequest(ByVal MyRequest As OMG_SubmitOrderRequest) As OMG_SubmitOrderResponse

        Dim MyResponse As New OMG_SubmitOrderResponse
        Dim MyTMProduct As New TMProductDetails
        Dim ProceedOrder As Boolean = False

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductId) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Get TM product details based on tm_merchantID and tm_productcode
                        MyTMProduct = MyTMFunction.GetTMProductDetails(MyMerchant.MerchantID, MyRequest.ProductId)

                        If MyTMProduct.ResponseCode = 0 And UCase(MyTMProduct.Status) = "ACTIVE" Then

                            '6. Checking user subscription before proceed to process TM billing order request
                            '- check in BillingSubscription_tbl based on tm_id (BillingValidation)
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation)                        
                            Dim ValidateBill As New PostResponse
                            ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                            If ValidateBill.ResponseCode = "0" Then

                                If ValidateBill.Status = "0" Then

                                    Dim ValidateProduct As New PostResponse
                                    ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID)

                                    If ValidateProduct.ResponseCode = "0" Then

                                        Select Case ValidateProduct.Status
                                            Case 0
                                                ProceedOrder = True
                                            Case 1
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Already subscribe to product"
                                            Case 3
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Pending termination"
                                            Case 5
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Active product subscription under the same merchant"
                                            Case Else
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error"
                                                ErrorMessage = "ProductValidation status: " & ValidateProduct.Status
                                        End Select
                                    Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Already subscribe to product."
                                End If
                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                            End If

                            '7. Process TM billing order request
                            '- Insert in BillingSubscription_tbl & ProductSubscription_tbl
                            '- Based on OTT partner nature, send real-time or waiting for OTT validation
                            If ProceedOrder = True Then

                                Dim SubscribeBill As New PostResponse
                                SubscribeBill = MyTMFunction.BillingSubscription(MyRequest, MyTMProduct.TMProductID, MyMerchant.MerchantID)

                                If SubscribeBill.ResponseCode = "0" Then

                                    If SubscribeBill.Status = "1" Then

                                        Dim SubsProductReq As New ProductSubsReq
                                        Dim SubsProductResp As New PostResponse

                                        SubsProductReq.ServiceID = GetAccountResp.ServiceID
                                        SubsProductReq.LoginID = GetAccountResp.LoginID
                                        SubsProductReq.OTT_UserID = ""
                                        SubsProductReq.OTTMerchantID = MyTMProduct.OTTMerchantID
                                        SubsProductReq.OMGProductID = MyTMProduct.OMGProductID
                                        SubsProductReq.TMBillID = SubscribeBill.TMBillID
                                        SubsProductReq.TMProductID = MyTMProduct.TMProductID
                                        SubsProductReq.TMMerchantID = MyTMProduct.TMMerchantID

                                        SubsProductResp = MyOMGFunction.ProductSubscription(SubsProductReq)

                                        If SubsProductResp.ResponseCode = "0" Then

                                            Select Case SubsProductResp.Status
                                                Case 0
                                                    MyResponse.ResponseCode = 106
                                                    MyResponse.ResponseMsg = "Failed transaction"
                                                Case 1
                                                    MyResponse.ResponseCode = 0
                                                    MyResponse.ResponseMsg = "Successful transaction"

                                                    Dim TempDate As DateTime = SubsProductResp.ActivationDate
                                                    Dim Format As String = "MM/dd/yyyy"
                                                    Dim ActivationDate As String = TempDate.ToString(Format)

                                                    MyResponse.OTT_ActivationDate = ActivationDate
                                                Case Else
                                                    MyResponse.ResponseCode = 106
                                                    MyResponse.ResponseMsg = "Failed transaction : " & SubsProductResp.Status
                                            End Select
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "ProductSubscription: " & SubsProductResp.ResponseMsg & "(" & SubsProductResp.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 106
                                        MyResponse.ResponseMsg = "Unsuccessful transaction"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "BillingSubscription: " & SubscribeBill.ResponseMsg & "(" & SubscribeBill.ResponseCode & ")"
                                End If
                            End If      'END ProceedOrder = True

                        Else
                            Select Case MyTMProduct.ResponseCode
                                Case 0, 1       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error - product"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.TMProductID = MyTMProduct.TMProductID
        APILog.EventName = "OMG_OrderRequest"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyTMFunction.TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="OTT termination order from SDP.")>
    Public Function OMG_TerminationRequest(ByVal MyRequest As OMG_SubmitTerminationRequest) As OMG_SubmitTerminationResponse

        Dim MyResponse As New OMG_SubmitTerminationResponse
        Dim MyTMProduct As New TMProductDetails
        Dim CalculateServiceDay As Boolean = False
        Dim ServiceDay As Integer
        Dim BillCycleDate As String = ""
        Dim ProceedTermination As Boolean = False
        Dim TerminateStatus As String = ""

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID, ProductID & TM_BillingCycleDate
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.ProductId) <> "") And (Trim(MyRequest.TM_BillingCycleDate) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Get TM product details based on tm_merchantID and tm_productcode
                        MyTMProduct = MyTMFunction.GetTMProductDetails(MyMerchant.MerchantID, MyRequest.ProductId)

                        If MyTMProduct.ResponseCode = 0 And UCase(MyTMProduct.Status) = "ACTIVE" Then

                            '6. Checking user subscription before proceed to process TM billing termination request
                            '- check in BillingSubscription_tbl based on tm_id (BillingValidation)
                            '- check in ProductSubscription_tbl based on omg_id (ProductValidation)
                            Dim ValidateBill As New PostResponse
                            Dim ValidateProduct As New PostResponse
                            ValidateBill = MyTMFunction.BillingValidation(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                            If ValidateBill.ResponseCode = "0" Then

                                If ValidateBill.Status = "1" Then

                                    ValidateProduct = MyOMGFunction.ProductValidation(GetAccountResp.LoginID, MyTMProduct.OMGProductID, "", True)

                                    If ValidateProduct.ResponseCode = "0" Then

                                        Select Case ValidateProduct.Status
                                            Case 0
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "NOT subscribe to product."
                                            Case 3
                                                MyResponse.ResponseCode = 106
                                                MyResponse.ResponseMsg = "Pending termination"
                                            Case 1, 4
                                                CalculateServiceDay = True
                                            Case Else
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error"
                                                ErrorMessage = "ProductValidation Status: " & ValidateProduct.Status
                                        End Select
                                    Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error"
                                        ErrorMessage = "ProductValidation: " & ValidateProduct.ResponseMsg & "(" & ValidateProduct.ResponseCode & ")"
                                    End If

                                ElseIf ValidateBill.Status = "2" Then
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Order is in process."
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Subscription for product " & MyRequest.ProductId & " NOT EXIST"
                                End If
                            Else
                                MyResponse.ResponseCode = 101
                                MyResponse.ResponseMsg = "Technical error"
                                ErrorMessage = "BillingValidation: " & ValidateBill.ResponseMsg & "(" & ValidateBill.ResponseCode & ")"
                            End If

                            '7. Calculate service day
                            If (CalculateServiceDay = True) And ((MyTMProduct.ProductType = "ALA-CARTE-ANNUALLY") Or (MyTMProduct.ProductType = "ALA-CARTE-MONTHLY")) Then

                                If (ValidateBill.TM_BillCycle = "0") And
                                    (ValidateBill.OSMActivationDate <> "0") And
                                    (MyRequest.TM_BillingCycleDate IsNot Nothing) Then

                                    'Condition A: Order from SDP, terminate from SDP
                                    ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, ValidateBill.OSMActivationDate, Integer.Parse(MyRequest.TM_BillingCycleDate))

                                ElseIf (ValidateBill.TM_BillCycle <> "0") And
                                        (ValidateBill.OSMActivationDate = "0") Then

                                    Dim activateDate As String = ValidateProduct.ActivationDate.ToString("MM/dd/yyyy")

                                    'Condition C: Order from OTT, terminate from OTT
                                    'Condition D: Order from OTT, terminate from SDP
                                    ServiceDay = MyOMGFunction.CalculateServiceDay(MyTMProduct.ProductType, MyTMProduct.TMProductName, activateDate, Integer.Parse(ValidateBill.TM_BillCycle))
                                Else
                                    ServiceDay = 0
                                End If

                                If ServiceDay <> 0 Then
                                    ProceedTermination = True
                                Else
                                    MyResponse.ResponseCode = 106
                                    MyResponse.ResponseMsg = "Service day is NULL"
                                End If
                            Else
                                MyResponse.ResponseCode = 106
                                MyResponse.ResponseMsg = "Product " & MyTMProduct.TMProductCode & ", Product type " & MyTMProduct.ProductType & " cannot be terminated"
                            End If      'END CalculateServiceDay = True

                            '8. Proceed termination process
                            If ProceedTermination = True Then

                                Dim BillTerminateResp As New PostResponse

                                If (ValidateBill.TM_BillCycle = "0") Then

                                    If MyRequest.TM_BillingCycleDate IsNot Nothing Then
                                        BillCycleDate = MyRequest.TM_BillingCycleDate
                                    End If
                                    BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "terminate", BillCycleDate)

                                ElseIf ValidateBill.TM_BillCycle <> "0" Then
                                    BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "terminate")
                                End If

                                If BillTerminateResp.ResponseCode = "0" Then

                                    If BillTerminateResp.Status = "1" Then

                                        Dim OTTMerchant As New MerchantDetails
                                        OTTMerchant = MyOMGFunction.GetMerchantDetails("", MyTMProduct.OTTMerchantID)

                                        If OTTMerchant.ResponseCode = 0 And UCase(OTTMerchant.Status) = "ACTIVE" Then

                                            Dim OTTTerminateResp As New PostResponse
                                            'OTTTerminateResp = MyOMGFunction.TerminationToOTT(OTTMerchant.MerchantName, OTTMerchant.MerchantPWD, GetAccountResp.LoginID, MyTMProduct.OMGProductCode, MyTMProduct.OMGProductID)

                                            OTTTerminateResp.ResponseCode = "0"

                                            If OTTTerminateResp.ResponseCode = "0" Then
                                                'TerminateStatus = OTTTerminateResp.TerminateStatus
                                                TerminateStatus = "inactive"
                                            Else
                                                TerminateStatus = "pending_termination"
                                            End If
                                        Else
                                            TerminateStatus = "pending_termination"
                                        End If

                                        Dim ProdTerminateResp As New PostResponse
                                        ProdTerminateResp = MyOMGFunction.ProductTermination(MyTMProduct.OMGProductID, GetAccountResp.LoginID, TerminateStatus, "billing", ServiceDay)

                                        If ProdTerminateResp.ResponseCode = "0" And ProdTerminateResp.Status = "1" Then
                                            MyResponse.ResponseCode = 0
                                            MyResponse.ResponseMsg = "Successful Transaction"
                                        Else
                                            'Termination to OTT OK, but failed product termination
                                            MyResponse.ResponseCode = 106
                                            MyResponse.ResponseMsg = "Unsuccessful termination"
                                            ErrorMessage = "ProductTermination: " & ProdTerminateResp.ResponseMsg & "(" & ProdTerminateResp.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 101
                                        MyResponse.ResponseMsg = "Technical error - Stored prod BillingTermination"
                                        ErrorMessage = "BillingTermination Status: " & BillTerminateResp.Status
                                    End If
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "BillingTermination: " & BillTerminateResp.ResponseMsg & "(" & BillTerminateResp.ResponseCode & ")"
                                End If
                            End If       'END ProceedTermination = True

                        Else
                            Select Case MyTMProduct.ResponseCode
                                Case 0, 1       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error - product"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.TMProductID = MyTMProduct.TMProductID
        APILog.EventName = "OMG_TerminationRequest"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString & " - ServiceDay: " & ServiceDay

        If (MyResponse.ResponseCode = "101") Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyTMFunction.TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To send OTT order creation status to OMG.")>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)>
    Public Sub SubmitOrderResponse(ByVal OrderNumber As String, ByVal Status As String,
                ByVal ProductId As String, ByVal TM_AccountID As String,
                ByVal ResponseCode As String, ByVal ResponseMsg As String)

        Dim ProceedTermination As Boolean = False
        Dim ProcessStatus As String = "Success"
        Dim TerminateStatus As String = ""

        'IMPORTANT: Please dont change the ErrorMessage, this is standardize with NOVA / ICP
        Dim ErrorMessage() As String =
                {"Cannot create order. Asset is not Exist.",
                 "Cannot create order. Asset is not Active.",
                 "Cannot create order. Asset is Suspended.",
                 "Cannot create order.Existing OTT asset is Active.",
                 "Cannot Create Order.Active OTT asset is not exist.",
                 "Cannot create order.There is open order found."}

        '1. Get TM product details based on tm_productcode
        Dim MyTMProduct As New TMProductDetails
        MyTMProduct = MyTMFunction.GetTMProductDetails("", ProductId)

        If MyTMProduct.ResponseCode = 0 And UCase(MyTMProduct.Status) = "ACTIVE" Then

            '2. Check TM_AccountID
            Dim GetAccountResp As New AccountDetails
            GetAccountResp = MyOMGFunction.GetAccount(TM_AccountID)

            If GetAccountResp.ServiceID <> "-" Then

                '3. Update the status of Order request
                Dim UpdateResp As New PostResponse

                If (ResponseCode = "0") And (UCase(Status) = "SUCCESS") Then

                    UpdateResp = MyTMFunction.Update_NewOTTOrder(GetAccountResp.ServiceID, MyTMProduct.TMProductID, OrderNumber)

                    If UpdateResp.ResponseCode = "0" Then
                        Select Case UpdateResp.Status
                            Case 1  'Successfully change status from "Processing" to "Active".

                                Dim IB_Func As New InfoBlast_function
                                Dim SendSMSReq As New INT_SendSMSReq
                                Dim MyResponse As New INT_SendSMSResp

                                Dim strMsg As String = "RM0.00 TM: TQ for subscribing to " & MyTMProduct.TMProductName & ". Order ID : " & OrderNumber & " has been completed."

                                SendSMSReq.TM_AccountID = GetAccountResp.ServiceID
                                SendSMSReq.MobileNumber = UpdateResp.MobileNumber
                                SendSMSReq.ProductId = ProductId
                                SendSMSReq.StrMsg = strMsg

                                MyResponse = IB_Func.SMSInfoBlast(SendSMSReq, OrderNumber)

                            Case 0, 2
                                ProcessStatus = "Failed 1: " & UpdateResp.ErrorMsg
                        End Select
                    Else
                        ProcessStatus = "Failed 2: " & UpdateResp.ResponseMsg
                    End If

                ElseIf (ResponseCode = "0") And (UCase(Status) = "FAILED") Then

                    Select Case ResponseMsg

                        Case ErrorMessage(0), ErrorMessage(1)
                            ProcessStatus = "Failed 3: " & ResponseCode & " - " & ResponseMsg

                        Case ErrorMessage(2), ErrorMessage(5)
                            'Do nothing (DO NOT TERMINATE THE SERVICE)

                        Case ErrorMessage(3), ErrorMessage(4)

                            UpdateResp = MyTMFunction.Update_NewOTTOrder(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                            If UpdateResp.ResponseCode = "0" Then
                                Select Case UpdateResp.Status
                                    Case 1  'Successfully change status from "Processing" to "Active".
                                        ProceedTermination = True
                                    Case 0, 2
                                        ProcessStatus = "Failed 4: " & UpdateResp.ErrorMsg
                                End Select
                            Else
                                ProcessStatus = "Failed 5: " & UpdateResp.ResponseMsg
                            End If
                    End Select
                Else
                    'Condition: ResponseCode = null & Status = FAILED
                    If (UCase(Status) = "FAILED") And (ResponseMsg = ErrorMessage(3)) Then

                        UpdateResp = MyTMFunction.Update_NewOTTOrder(GetAccountResp.ServiceID, MyTMProduct.TMProductID)

                        If UpdateResp.ResponseCode = "0" Then
                            Select Case UpdateResp.Status
                                Case 1  'Successfully change status from "Processing" to "Active".
                                    ProceedTermination = True
                                Case 0, 2
                                    ProcessStatus = "Failed 12: " & UpdateResp.ErrorMsg
                            End Select
                        Else
                            ProcessStatus = "Failed 13: " & UpdateResp.ResponseMsg
                        End If
                    Else
                        ProcessStatus = "Failed 6: " & ResponseCode & " - " & ResponseMsg
                    End If
                End If
            Else
                ProcessStatus = "Failed 14: Invalid TM_AccountID"
            End If

            If ProceedTermination = True Then

                Dim BillTerminateResp As New PostResponse
                BillTerminateResp = MyTMFunction.BillingTermination(GetAccountResp.ServiceID, MyTMProduct.TMProductID, "Terminate")

                If BillTerminateResp.ResponseCode = "0" Then

                    If BillTerminateResp.Status = "1" Then

                        Dim OTTMerchant As New MerchantDetails
                        OTTMerchant = MyOMGFunction.GetMerchantDetails("", MyTMProduct.OTTMerchantID)

                        If OTTMerchant.ResponseCode = 0 And UCase(OTTMerchant.Status) = "ACTIVE" Then

                            Dim OTTTerminateResp As New PostResponse
                            'OTTTerminateResp = MyOMGFunction.TerminationToOTT(OTTMerchant.MerchantName, OTTMerchant.MerchantPWD, GetAccountResp.LoginID, MyTMProduct.OMGProductCode, MyTMProduct.OMGProductID)

                            OTTTerminateResp.ResponseCode = "0"

                            If OTTTerminateResp.ResponseCode = "0" Then
                                'TerminateStatus = OTTTerminateResp.TerminateStatus
                                TerminateStatus = "inactive"
                            Else
                                TerminateStatus = "pending_termination"
                            End If
                        Else
                            TerminateStatus = "pending_termination"
                        End If

                        Dim ProdTerminateResp As New PostResponse
                        ProdTerminateResp = MyOMGFunction.ProductTermination(MyTMProduct.OMGProductID, GetAccountResp.LoginID, TerminateStatus, "billing")

                        If ProdTerminateResp.ResponseCode = "0" And ProdTerminateResp.Status = "1" Then
                            ProcessStatus = "Success"
                        Else
                            'Termination to OTT OK, but failed product termination
                            If ProdTerminateResp.ResponseCode = "0" Then
                                ProcessStatus = "Failed 7: " & ProdTerminateResp.ResponseCode & " - Status " & ProdTerminateResp.Status
                            Else
                                ProcessStatus = "Failed 8: " & ProdTerminateResp.ResponseCode & " - " & ProdTerminateResp.ResponseMsg
                            End If
                        End If
                    Else
                        ProcessStatus = "Failed 9: " & BillTerminateResp.ResponseCode & " - Status " & BillTerminateResp.Status
                    End If
                Else
                    ProcessStatus = "Failed 10: " & BillTerminateResp.ResponseCode & " - " & BillTerminateResp.ResponseMsg
                End If
            End If
        Else
            ProcessStatus = "Failed 11: " & MyTMProduct.ResponseCode & " - " & MyTMProduct.ResponseMsg
        End If

        'Restructure response from NOVA/ICP, to insert into database
        Dim ReceiveResp As New INT_SubmitOrderResponse
        ReceiveResp.OrderNumber = OrderNumber
        ReceiveResp.Status = Status
        ReceiveResp.ProductId = ProductId
        ReceiveResp.TM_AccountID = TM_AccountID
        ReceiveResp.ResponseCode = ResponseCode
        ReceiveResp.ResponseMsg = ResponseMsg
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(ReceiveResp, ReceiveResp.GetType)

        APILog.TM_AccountID = TM_AccountID
        APILog.TMProductID = MyTMProduct.TMProductID
        APILog.EventName = "SubmitOrderResponse"
        APILog.DataResp = MyResponseString
        APILog.ProcessStatus = ProcessStatus
        MyTMFunction.TMBillingAPILog(APILog)
    End Sub

    '============================================================================================================================================
    'FOR INTERNAL USE
    '============================================================================================================================================
    <WebMethod(Description:="To validate TM account ID based on identification type.")>
    Public Function OMG_ValidateAccount(ByVal MyRequest As OMG_ValidateUserReq) As OMG_ValidateUserResp

        Dim MyResponse As New OMG_ValidateUserResp
        Dim ValidateUserReq As New INT_ValidateUserReq
        Dim ValidateUserResp As New INT_ValidateUserResp

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" And UCase(MyMerchant.MerchantType) = "INTERNAL" Then

            '2. Check TM_AccountID IS NOT null
            If Trim(MyRequest.TM_AccountID) <> "" Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4.Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID, MyRequest.AccountType, MyRequest.ClientType)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Request account validation to NOVA / ICP
                        If GetAccountResp.ClientType = "STB" Then
                            ValidateUserReq.TM_AccountID = GetAccountResp.SubscriberID
                        Else
                            ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
                        End If

                        ValidateUserReq.TM_AccountType = GetAccountResp.AccountType
                        ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)

                        If ValidateUserResp.ResponseCode = "0" Then

                            If UCase(ValidateUserResp.Status) = "ACTIVE" Then

                                If (MyRequest.Id IsNot Nothing) And (MyRequest.IdType IsNot Nothing) Then

                                    If (MyRequest.Id = ValidateUserResp.Id) And (MyRequest.IdType = ValidateUserResp.IdType) Then

                                        Dim SetIdReq As New INT_SetTransactionIDReq
                                        Dim SetIdResp As New INT_SetTransactionIDResp

                                        SetIdReq.TM_AccountID = ValidateUserResp.TMAccountID
                                        SetIdReq.MobileNumber = ValidateUserResp.MobileNumber
                                        SetIdReq.CustomerEmail = ValidateUserResp.CustomerEmail
                                        SetIdReq.Id = ValidateUserResp.Id
                                        SetIdReq.IdType = ValidateUserResp.IdType
                                        SetIdReq.TM_BillCycle = ValidateUserResp.TM_BillCycle

                                        '6. Create OMG_TXNID - purpose to proceed with OMG_NewOTTOrder
                                        SetIdResp = MyTMFunction.SetOMGTransactionID(SetIdReq)

                                        If SetIdResp.ResponseCode = "0" Then

                                            If SetIdResp.Status = "1" Then
                                                MyResponse.TM_AccountID = ValidateUserResp.TMAccountID
                                                MyResponse.OMG_TXNID = SetIdResp.OMG_TXNID
                                                MyResponse.ResponseCode = 0
                                                MyResponse.ResponseMsg = "Successful validation"
                                            Else
                                                MyResponse.ResponseCode = 101
                                                MyResponse.ResponseMsg = "Technical error"
                                                ErrorMessage = "SetOMGTransactionID Status: " & SetIdResp.Status
                                            End If
                                        Else
                                            MyResponse.ResponseCode = 101
                                            MyResponse.ResponseMsg = "Technical error"
                                            ErrorMessage = "OMGID: " & SetIdResp.ResponseMsg & "(" & SetIdResp.ResponseCode & ")"
                                        End If
                                    Else
                                        MyResponse.ResponseCode = 103
                                        MyResponse.ResponseMsg = "Identification NOT match"
                                    End If
                                Else
                                    MyResponse.TM_AccountID = ValidateUserResp.TMAccountID
                                    MyResponse.STBId = ValidateUserResp.STBId

                                    MyResponse.TM_AccountType = ValidateUserResp.TM_AccountType
                                    MyResponse.ServiceNumber = ValidateUserResp.ServiceNumber
                                    MyResponse.BundleName = ValidateUserResp.BundleName
                                    MyResponse.ProductName = ValidateUserResp.ProductName
                                    MyResponse.FullName = ValidateUserResp.AccountName
                                    MyResponse.MobileNumber = ValidateUserResp.MobileNumber
                                    MyResponse.CustomerEmail = ValidateUserResp.CustomerEmail
                                    MyResponse.Id = ValidateUserResp.Id
                                    MyResponse.IdType = ValidateUserResp.IdType

                                    Dim MyAddress As New Address
                                    MyAddress.HouseNo = ValidateUserResp.HouseNo
                                    MyAddress.FloorNo = ValidateUserResp.FloorNo
                                    MyAddress.BuildingName = ValidateUserResp.BuildingName
                                    MyAddress.StreetType = ValidateUserResp.StreetType
                                    MyAddress.StreetName = ValidateUserResp.StreetName
                                    MyAddress.Section = ValidateUserResp.Section
                                    MyAddress.PostalCode = ValidateUserResp.PostalCode
                                    MyAddress.City = ValidateUserResp.City
                                    MyAddress.StateName = ValidateUserResp.StateName
                                    MyAddress.Country = ValidateUserResp.Country

                                    MyResponse.Address = MyAddress
                                    MyResponse.ResponseCode = 0
                                    MyResponse.ResponseMsg = "Successful transaction"
                                    'MyResponse.ResponseMsg = ValidateUserResp.ResponseMsg
                                End If
                            Else
                                MyResponse.ResponseCode = 106
                                MyResponse.ResponseMsg = "Account is NOT active"
                            End If

                        ElseIf ValidateUserResp.ResponseCode = "103" Then
                            MyResponse.ResponseCode = 106
                            MyResponse.ResponseMsg = "Account is NOT active"

                        ElseIf ValidateUserResp.ResponseCode = "106" Then
                            MyResponse.ResponseCode = 106
                            MyResponse.ResponseMsg = "Account NOT Exist"
                        Else
                            MyResponse.ResponseCode = 101
                            MyResponse.ResponseMsg = "Technical error"
                            ErrorMessage = "ValidateAccountId: " & ValidateUserResp.ResponseMsg & "(" & ValidateUserResp.ResponseCode & ")"
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.EventName = "OMG_ValidateAccount"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Then
            APILog.ProcessStatus = ErrorMessage
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To generate OTP PIN")>
    Function OMG_GenerateOTP(ByVal MyRequest As OMG_SetOTPReq) As OMG_SetOTPResp

        Dim MyResponse As New OMG_SetOTPResp
        Dim MyOMGProduct As New OMGProductDetails
        Dim SendByEmail As Boolean = True       'By Default send by mail (NO cost)

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" And UCase(MyMerchant.MerchantType) = "INTERNAL" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.OTT_TXNID) <> "") And (Trim(MyRequest.ProductID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product exist & active
                        MyOMGProduct = MyOMGFunction.GetOMGProductDetails(MyMerchant.MerchantID, MyRequest.ProductID)

                        If MyOMGProduct.ResponseCode = 0 And UCase(MyOMGProduct.Status) = "ACTIVE" And MyOMGProduct.OMGProductType = "OTP" Then

                            If MyRequest.FullOTPFlag Is Nothing Then
                                MyRequest.FullOTPFlag = "Y"
                            End If

                            If ((Trim(MyRequest.MobileNumber) <> "") And (Not MyRequest.MobileNumber Is Nothing)) Or (Not MyRequest.CustomerEmail Is Nothing) Then

                                If (Trim(MyRequest.MobileNumber) <> "") And (Not MyRequest.MobileNumber Is Nothing) Then
                                    SendByEmail = False
                                End If

                                '7. Generate Verification code
                                Dim GenerateVCodeReq As New INT_SetVCodeReq
                                Dim GenerateVCodeResp As New PostResponse

                                GenerateVCodeReq.OTT_ID = MyRequest.OTT_ID
                                GenerateVCodeReq.OTT_TXNID = MyRequest.OTT_TXNID
                                GenerateVCodeReq.TM_AccountID = MyRequest.TM_AccountID
                                GenerateVCodeReq.ProductID = MyRequest.ProductID

                                If SendByEmail = True Then
                                    GenerateVCodeReq.MobileNumber = "-"
                                    GenerateVCodeReq.CustomerEmail = MyRequest.CustomerEmail
                                Else
                                    GenerateVCodeReq.MobileNumber = MyRequest.MobileNumber
                                    GenerateVCodeReq.CustomerEmail = "-"
                                End If

                                GenerateVCodeReq.TM_BillCycle = 0
                                GenerateVCodeReq.VCodeValidity = MyMerchant.CodeValidity

                                GenerateVCodeResp = MyOMGFunction.SetVCode(GenerateVCodeReq)

                                If GenerateVCodeResp.ResponseCode = "0" And GenerateVCodeResp.Status = "1" Then

                                    '8. Send Verification code
                                    Dim strMsg As String = ""
                                    Dim OTPSend As String = ""

                                    If UCase(MyRequest.FullOTPFlag) = "Y" Then
                                        OTPSend = GenerateVCodeResp.InitialVCode & "-" & GenerateVCodeResp.VCode
                                    Else
                                        OTPSend = GenerateVCodeResp.VCode
                                    End If

                                    Dim ValidityTime As String = ""

                                    If MyMerchant.CodeValidity <> 0 And (Not GenerateVCodeResp.ValidityEndDate.ToString Is Nothing) Then

                                        Dim TempDate As DateTime = GenerateVCodeResp.ValidityEndDate
                                        'Dim Format As String = "ddd MMM dd HH:mm:ss yyyy"
                                        Dim Format As String = "dd/MM/yyyy hh:mm:ss tt"
                                        ValidityTime = TempDate.ToString(Format)

                                        strMsg = "TM: unifi TV One-time PIN: " & OTPSend & ". Expires by " & ValidityTime & ". TQ"
                                    Else
                                        strMsg = "TM: unifi TV One-time PIN: " & OTPSend & ". TQ"
                                    End If

                                    If SendByEmail = True Then      'Send by mail

                                        Dim EmailSubject As String = "unifi TV Verification Code"

                                        If MyRequest.OTT_ID = "epgpin" Then

                                            Dim arrInfo() As String
                                            arrInfo = MyRequest.AdditionalInfo.Split(",")

                                            EmailSubject = arrInfo(0)

                                            strMsg = "Hi <strong>" & MyRequest.TM_AccountID & "</strong>, <br><br> Your " & arrInfo(1) & " verification code is <strong>" & OTPSend & "</strong>. "
                                            If arrInfo(3) = "V6R1" Then
                                                strMsg += "You may reset your " & arrInfo(1) & " at Menu > Profile Settings > Manage PIN > " & arrInfo(2) & ". <br><br>"
                                            Else
                                                strMsg += "You may reset your " & arrInfo(1) & " at Menu > Setting > Profiles > " & arrInfo(2) & ". <br><br>"
                                            End If
                                            strMsg += "Verification codes are valid until " & ValidityTime & ". <br> This message was generated automatically. "
                                            strMsg += "<br><br> Sincerely, <br> unifi TV <br><br><i>If you need help or have questions, please contact us at <a href='http://unifi.com.my/chat'>http://unifi.com.my/chat<a></i>"

                                        End If

                                        Dim MyMailFunction As New Mail_Function

                                        Dim SendMailReq As New INT_SendMailReq
                                        Dim SendMailResp As New INT_SendMailResp

                                        If MyRequest.CustomerEmail Is Nothing Then
                                            SendMailReq.Email = "haidah.hussaini@tm.com.my"
                                            SendMailReq.Subject = "unifi TV Verification Code " & MyRequest.TM_AccountID
                                        Else
                                            SendMailReq.Email = MyRequest.CustomerEmail
                                            SendMailReq.Subject = EmailSubject
                                        End If
                                        SendMailReq.MsgBody = strMsg

                                        SendMailResp = MyMailFunction.SendViaEmail(SendMailReq)

                                        If SendMailResp.ResponseMsg = "OK" Then

                                            MyResponse.OTT_ID = MyRequest.OTT_ID
                                            If MyRequest.OTT_TXNID.Length > 15 Then
                                                MyResponse.OTT_TXNID = MyRequest.OTT_TXNID.Substring(0, 15)
                                            Else
                                                MyResponse.OTT_TXNID = MyRequest.OTT_TXNID
                                            End If

                                            MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                            MyResponse.InitialVCode = GenerateVCodeResp.InitialVCode

                                            Dim strEmail() As String
                                            strEmail = SendMailReq.Email.Split("@")

                                            Dim MLength As Integer = strEmail(0).Length
                                            Dim Asteric As String = ""

                                            Asteric = Left(strEmail(0), 2)
                                            For value As Integer = 0 To MLength - 4
                                                Asteric = Asteric & "*"
                                            Next
                                            Asteric = Asteric & Right(strEmail(0), 1)
                                            Dim TempEmail As String = Asteric & "@" & strEmail(1)

                                            MyResponse.CustomerEmail = TempEmail

                                            MyResponse.ResponseCode = 0
                                            MyResponse.ResponseMsg = "Successful transaction"
                                        Else
                                            MyResponse.ResponseCode = 106
                                            MyResponse.ResponseMsg = "Failed transaction"
                                            ErrorMessage = "SendViaEmail: " & SendMailResp.ResponseMsg & "(" & SendMailResp.ResponseCode & ")"
                                        End If
                                    Else
                                        Dim SMS_Func As New SMS_Function

                                        Dim SendSMSReq As New INT_SendSMSReq
                                        Dim SendSMSResp As New GeneralResponse

                                        SendSMSReq.TM_AccountID = MyRequest.TM_AccountID
                                        SendSMSReq.MobileNumber = MyRequest.MobileNumber
                                        SendSMSReq.ProductId = MyRequest.ProductID
                                        SendSMSReq.StrMsg = strMsg

                                        SendSMSResp = SMS_Func.SendSMS62100(SendSMSReq, MyRequest.OTT_TXNID, "", GenerateVCodeResp.InitialVCode, GenerateVCodeResp.VCode)

                                        If SendSMSResp.ResponseCode = "0" Then

                                            MyResponse.OTT_ID = MyRequest.OTT_ID

                                            If MyRequest.OTT_TXNID.Length > 15 Then
                                                MyResponse.OTT_TXNID = MyRequest.OTT_TXNID.Substring(0, 15)
                                            Else
                                                MyResponse.OTT_TXNID = MyRequest.OTT_TXNID
                                            End If
                                            MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                            MyResponse.InitialVCode = GenerateVCodeResp.InitialVCode

                                            Dim MLength As Integer = MyRequest.MobileNumber.Length - 4
                                            Dim Asteric As String = ""

                                            For value As Integer = 0 To MLength - 1
                                                Asteric = Asteric & "*"
                                            Next
                                            Dim LastFourDigits As String = MyRequest.MobileNumber.Substring(MLength, 4)
                                            Dim TempMobileNo As String = Asteric & LastFourDigits

                                            MyResponse.MobileNumber = TempMobileNo
                                            MyResponse.ResponseCode = 0
                                            MyResponse.ResponseMsg = "Successful transaction"

                                        Else
                                            MyResponse.ResponseCode = 106
                                            MyResponse.ResponseMsg = "Failed transaction"
                                            ErrorMessage = "SendSMS62100: " & SendSMSResp.ResponseMsg & "(" & SendSMSResp.ResponseCode & ")"
                                        End If

                                    End If
                                Else
                                    MyResponse.ResponseCode = 106

                                    If GenerateVCodeResp.Status = "2" Then
                                        MyResponse.ResponseMsg = "Duplicate OTT_TXNID"
                                    Else
                                        MyResponse.ResponseMsg = "Failed transaction"
                                        ErrorMessage = "SetVCode: " & GenerateVCodeResp.ResponseMsg & "(" & GenerateVCodeResp.ResponseCode & ")"
                                    End If
                                End If

                            Else    'Mobile number / email is null
                                MyResponse.ResponseCode = 107
                                MyResponse.ResponseMsg = "Invalid Mobile number / email"
                            End If

                        Else
                            Select Case MyOMGProduct.ResponseCode
                                Case 0, 1       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg & "(" & MyOMGProduct.ResponseCode & ")"
                            End Select
                        End If
                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyOMGProduct.OMGProductID
        APILog.EventName = "OMG_GenerateOTP"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To verify OTP PIN")>
    Function OMG_VerifyOTP(ByVal MyRequest As OMG_GetOTPReq) As OMG_GetOTPResp

        Dim MyResponse As New OMG_GetOTPResp
        Dim MyOMGProduct As New OMGProductDetails

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" And UCase(MyMerchant.MerchantType) = "INTERNAL" Then

            '2. Check request contain TM_AccountID & ProductID
            If (Trim(MyRequest.TM_AccountID) <> "") And (Trim(MyRequest.OTT_TXNID) <> "") And (Trim(MyRequest.ProductID) <> "") And (Trim(MyRequest.OTPCode) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    '4. Check TM_AccountID
                    Dim GetAccountResp As New AccountDetails
                    GetAccountResp = MyOMGFunction.GetAccount(MyRequest.TM_AccountID)

                    If GetAccountResp.ServiceID <> "-" Then

                        '5. Check product exist & active
                        MyOMGProduct = MyOMGFunction.GetOMGProductDetails(MyMerchant.MerchantID, MyRequest.ProductID)

                        If MyOMGProduct.ResponseCode = 0 And UCase(MyOMGProduct.Status) = "ACTIVE" And MyOMGProduct.OMGProductType = "OTP" Then

                            'Check if verification code format send is TRUE
                            Dim vcodeSend As Boolean = MyRequest.OTPCode Like "[A-Z][A-Z][A-Z][A-Z][ -.]######"

                            If vcodeSend = True Then

                                Dim GetVCodeResp As New INT_GetVCodeResp
                                GetVCodeResp = MyOMGFunction.GetVCode(MyRequest.OTPCode)

                                'Status = 0: FAILED, 1: SUCCESS, 2: INVALID, 3: EXPIRED

                                If GetVCodeResp.ResponseCode <> "-2" Then

                                    MyResponse.ResponseCode = 0
                                    MyResponse.ResponseMsg = "Successful transaction"
                                    MyResponse.Status = GetVCodeResp.ResponseCode

                                    If GetVCodeResp.ResponseCode = "1" And GetVCodeResp.Status = "VALID" Then
                                        If (MyRequest.OTT_ID = GetVCodeResp.OTT_ID) And
                                            (MyRequest.OTT_TXNID = GetVCodeResp.OTT_TXNID) And (MyRequest.TM_AccountID = GetVCodeResp.TM_AccountID) And
                                            (MyRequest.ProductID = GetVCodeResp.ProductID) Then

                                            MyResponse.OTT_ID = MyRequest.OTT_ID
                                            MyResponse.TM_AccountID = MyRequest.TM_AccountID
                                            MyResponse.OTPCode = MyRequest.OTPCode
                                        Else
                                            MyResponse.ResponseCode = 108
                                            MyResponse.ResponseMsg = "Failed validation"
                                        End If
                                    Else
                                        ErrorMessage = "GetVCode " & GetVCodeResp.ResponseMsg & "(" & GetVCodeResp.ResponseCode & ")"
                                    End If
                                Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "GetVCode: " & MyResponse.ResponseMsg
                                End If

                            Else
                                MyResponse.ResponseCode = 107
                                MyResponse.ResponseMsg = "Invalid verification code format"
                                ErrorMessage = "Invalid verification code format - " & MyRequest.OTPCode
                            End If

                        Else
                            Select Case MyOMGProduct.ResponseCode
                                Case 0, 1       'INACTIVE & NOT EXIST
                                    MyResponse.ResponseCode = 103
                                    MyResponse.ResponseMsg = "Product ID not exist"
                                Case 2
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg
                                Case Else
                                    MyResponse.ResponseCode = 101
                                    MyResponse.ResponseMsg = "Technical error"
                                    ErrorMessage = "OMG: " & MyOMGProduct.ResponseMsg & "(" & MyOMGProduct.ResponseCode & ")"
                            End Select
                        End If

                    Else
                        MyResponse.ResponseCode = 104
                        MyResponse.ResponseMsg = "Invalid TM_AccountID"
                    End If
                Else
                    MyResponse.ResponseCode = "100"
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.OMGProductID = MyOMGProduct.OMGProductID
        APILog.EventName = "OMG_VerifyOTP"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If MyResponse.ResponseCode = "101" Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    <WebMethod(Description:="To get subscription status based on Product ID.")>
    Public Function OMG_ValidateProductStatus(ByVal MyRequest As OMG_GetProductStatusReq) As OMG_GetProductStatusResp

        Dim MyResponse As New OMG_GetProductStatusResp

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            '2. Check request contain TM_AccountID
            If (Trim(MyRequest.SubscriberID) <> "") Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.SubscriberID + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '3. Check matching signature
                If MySignature = MyRequest.Signature Then

                    Dim INTStatusReq As New INT_SubscriptionStatusReq
                    INTStatusReq.SubscriberID = MyRequest.SubscriberID

                    Dim x As Integer = 0

                    Dim INTProductListReq As New ProductList
                    Dim INTProductReq(MyRequest.ProductList.Total) As Product

                    For x = 0 To (MyRequest.ProductList.Total - 1)
                        Dim Product_x As New Product()
                        Product_x.Id = MyRequest.ProductList.Product(x).Id
                        INTProductReq(x) = Product_x
                    Next
                    INTProductListReq.Product = INTProductReq
                    INTProductListReq.Total = MyRequest.ProductList.Total
                    INTStatusReq.ProductList = INTProductListReq

                    MyResponse = MyTMFunction.GetSubscriptionStatus(INTStatusReq)
                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                MyResponse.ResponseCode = 105
                MyResponse.ResponseMsg = "Missing parameter"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.SubscriberID
        APILog.EventName = "OMG_ValidateProductStatus"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If (MyResponse.ResponseCode = "101") Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyOMGFunction.OMGAPILog(APILog)

        Return MyResponse
    End Function

    '============================================================================================================================================
    'TO USE WHEN RECEIVE ERROR FROM SIEBEL
    '============================================================================================================================================
    <WebMethod(Description:="To retry create order for new OTT product subscription.")>
    Public Function OMG_RetryOTTOrder(ByVal MyRequest As OMG_RetryOTTOrderReq) As OMG_RetryOTTOrderResp

        'Notes:
        'ReqType: can only be "Order" or "Terminate"

        Dim MyResponse As New OMG_RetryOTTOrderResp
        Dim MyOTTOrderReq As New INT_OTTOrderReq
        Dim MyOTTOrderResp As New INT_OTTOrderResp

        MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

        '1. Check merchant exist & active
        If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" Then

            b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "##" + MyRequest.TM_AccountID + "&&"
            MySignature = MyOMGFunction.GetEncryption(b4hashstring)

            '2. Check matching signature
            If MySignature = MyRequest.Signature Then

                '3. Get subscription order info
                MyOTTOrderReq.TMBill_ID = MyRequest.TMBill_ID
                MyOTTOrderReq.TM_AccountID = MyRequest.TM_AccountID

                MyOTTOrderResp = MyTMFunction.GetOTTOrder(MyOTTOrderReq)

                If MyOTTOrderResp.ResponseCode = 0 And
                    ((MyRequest.ReqType = "Order" And MyOTTOrderResp.Status = "PROCESSING") Or
                     MyRequest.ReqType = "Terminate" And MyOTTOrderResp.Status = "ACTIVE") Then

                    Dim TempDate As DateTime = DateTime.Now
                    Dim Format As String = "MM/dd/yyyy"
                    Dim TransactionDate As String = TempDate.ToString(Format)

                    Dim TMOrderReq As New INT_NewOrderReq
                    Dim TMOrderResp As New INT_NewOrderResp

                    '-- Need to enable when production -----------------------------
                    If MyRequest.ReqType = "Order" Then

                        TMOrderReq.ReqType = "Order"
                        TMOrderReq.TM_AccountID = MyOTTOrderResp.TM_AccountID
                        TMOrderReq.TM_AccountType = MyOTTOrderResp.TM_AccountType
                        TMOrderReq.TMProductID = MyOTTOrderResp.TMProductID
                        TMOrderReq.TMProductCode = MyOTTOrderResp.TMProductCode
                        TMOrderReq.TMProductName = MyOTTOrderResp.TMProductName
                        TMOrderReq.ProductType = MyOTTOrderResp.ProductType
                        TMOrderReq.ActivationDate = TransactionDate

                    ElseIf MyRequest.ReqType = "Terminate" Then

                        TMOrderReq.ReqType = "Terminate"
                        TMOrderReq.TM_AccountID = MyOTTOrderResp.TM_AccountID
                        TMOrderReq.TM_AccountType = MyOTTOrderResp.TM_AccountType
                        TMOrderReq.TMProductID = MyOTTOrderResp.TMProductID
                        TMOrderReq.TMProductCode = MyOTTOrderResp.TMProductCode
                        TMOrderReq.TMProductName = MyOTTOrderResp.TMProductName
                        TMOrderReq.ProductType = MyOTTOrderResp.ProductType
                        TMOrderReq.DeactivationDate = TransactionDate
                    End If

                    TMOrderResp = MyTMFunction.NewOTTOrder(TMOrderReq)
                    '---------------------------------------------------------------

                    If TMOrderResp.ResponseCode = "0" Then

                        Dim RetrySubscribeBill As New PostResponse
                        RetrySubscribeBill = MyTMFunction.BillingSubscriptionRetry(MyOTTOrderResp.TM_AccountID, MyOTTOrderResp.TMProductID, MyRequest.TMBill_ID, MyRequest.ReqType)

                        If RetrySubscribeBill.ResponseCode = "0" And RetrySubscribeBill.Status = "1" Then
                            MyResponse.ResponseCode = 0
                            MyResponse.ResponseMsg = "Successful Transaction"
                        Else
                            MyResponse.ResponseCode = 0
                            MyResponse.ResponseMsg = "Failed Transaction"
                        End If
                    Else
                        MyResponse.ResponseCode = 106
                        MyResponse.ResponseMsg = "Failed order creation to TM Billing"
                    End If
                Else
                    MyResponse.ResponseCode = 103
                    MyResponse.ResponseMsg = "No Order to be process"
                End If
            Else
                MyResponse.ResponseCode = "100"
                MyResponse.ResponseMsg = "Signature does not match"
            End If
        Else
            Select Case MyMerchant.ResponseCode
                Case 0, 1       'INACTIVE & NOT EXIST
                    MyResponse.ResponseCode = 102
                    MyResponse.ResponseMsg = "Invalid OTT ID"
                Case 2
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error"
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg
                Case Else
                    MyResponse.ResponseCode = 101
                    MyResponse.ResponseMsg = "Technical error - " & MyMerchant.ResponseCode
                    ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
            End Select
        End If

        Dim DataToPost As String = MyOMGFunction.GetSerializeXML(MyRequest, MyRequest.GetType)
        Dim MyResponseString As String = MyOMGFunction.GetSerializeXML(MyResponse, MyResponse.GetType)

        APILog.TM_AccountID = MyRequest.TM_AccountID
        APILog.TMProductID = MyOTTOrderResp.TMProductID
        APILog.EventName = "OMG_RetryOTTOrder"
        APILog.DataReq = DataToPost
        APILog.DataResp = MyResponseString

        If (MyResponse.ResponseCode = "101") Or
            (MyResponse.ResponseCode <> "0" And ErrorMessage <> "") Then
            APILog.ProcessStatus = ErrorMessage

        ElseIf MyResponse.ResponseCode <> "0" Then
            APILog.ProcessStatus = MyResponse.ResponseCode & " - " & MyResponse.ResponseMsg
        End If
        MyTMFunction.TMBillingAPILog(APILog)

        Return MyResponse
    End Function

    '============================================================================================================================
    'For Teka Tekan
    '============================================================================================================================
    <WebMethod(Description:="To validate TM account ID and retrieve user information in bulk.")>
    Public Function OMG_RetrieveAccountDetail(ByVal MyRequest As OMG_AccountDetailRequest) As OMG_AccountDetailResponse

        Dim MyResponse As New OMG_AccountDetailResponse

        Try
            MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

            '1. Check merchant exist & active
            If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" And UCase(MyMerchant.MerchantType) = "INTERNAL" Then

                b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "&&"
                MySignature = MyOMGFunction.GetEncryption(b4hashstring)

                '2. Check matching signature
                If MySignature = MyRequest.Signature Then

                    MyResponse.ResponseCode = 0
                    MyResponse.ResponseMsg = "Successful transaction"
                Else
                    MyResponse.ResponseCode = 100
                    MyResponse.ResponseMsg = "Signature does not match"
                End If
            Else
                Select Case MyMerchant.ResponseCode
                    Case 0, 1       'INACTIVE & NOT EXIST
                        MyResponse.ResponseCode = 102
                        MyResponse.ResponseMsg = "Invalid OTT ID"
                    Case 2
                        MyResponse.ResponseCode = 101
                        MyResponse.ResponseMsg = "Technical error"
                        ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
                    Case Else
                        MyResponse.ResponseCode = 101
                        MyResponse.ResponseMsg = "Technical error"
                        ErrorMessage = "M: " & MyMerchant.ResponseMsg
                End Select
            End If

            Return MyResponse

        Catch ex As Exception
            'Do nothing
        Finally
            If MyResponse.ResponseCode = "0" Then
                Call MyTMFunction.BulkValidateAccountId(MyRequest.AccountIDs, MyRequest.showID)
            End If
        End Try

    End Function

    '<WebMethod(Description:="To send SMS.")> _
    'Public Function OMG_SendSMS(ByVal MyRequest As OMG_SendSMSReq) As OMG_SendSMSResp

    '    Dim MyResponse As New OMG_SendSMSResp

    '    Dim IB_Func As New InfoBlast_function

    '    Dim MyTMProduct As New TMProductDetails
    '    MyTMProduct = MyTMFunction.GetTMProductDetails("", MyRequest.ProductId)

    '    If MyTMProduct.ResponseCode = 0 And UCase(MyTMProduct.Status) = "ACTIVE" Then

    '        'MyResponse = IB_Func.SMSInfoBlast(MyRequest.TM_AccountID, MyRequest.MobileNumber, MyRequest.OrderNumber, MyRequest.ProductId, MyTMProduct.TMProductName)

    '        Dim SendSMSReq As New INT_SendSMSReq
    '        Dim SendSMSResp As New INT_SendSMSResp

    '        Dim strMsg As String = "RM0.00 TM: TQ for subscribing to " & MyTMProduct.TMProductName

    '        SendSMSReq.TM_AccountID = MyRequest.TM_AccountID
    '        SendSMSReq.MobileNumber = MyRequest.MobileNumber
    '        SendSMSReq.ProductId = MyTMProduct.TMProductCode
    '        SendSMSReq.StrMsg = strMsg

    '        SendSMSResp = IB_Func.SMSInfoBlast(SendSMSReq)

    '        MyResponse.ResponseCode = SendSMSResp.ResponseCode
    '        MyResponse.ResponseMsg = SendSMSResp.ResponseMsg

    '    Else
    '        Select Case MyTMProduct.ResponseCode
    '            Case 0, 1       'INACTIVE & NOT EXIST
    '                MyResponse.ResponseCode = 103
    '                MyResponse.ResponseMsg = "Product ID not exist"
    '            Case 2
    '                MyResponse.ResponseCode = 101
    '                MyResponse.ResponseMsg = "Technical error - product"
    '                ErrorMessage = "TM: " & MyTMProduct.ResponseMsg
    '            Case Else
    '                MyResponse.ResponseCode = 101
    '                MyResponse.ResponseMsg = "Technical error"
    '                ErrorMessage = "TM: " & MyTMProduct.ResponseMsg & "(" & MyTMProduct.ResponseCode & ")"
    '        End Select
    '    End If

    '    Return MyResponse
    'End Function

    '<WebMethod(Description:="To send any notification through SMS or email.")>
    'Public Function OMG_SendNotifications(ByVal MyRequest As OMG_SendNotifyReq) As OMG_SendNotifyResp

    '    Dim MyResponse As New OMG_SendNotifyResp
    '    Dim MyMailFunction As New Mail_Function

    '    MyMerchant = MyOMGFunction.GetMerchantDetails(MyRequest.OTT_ID, "")

    '    '1. Check merchant exist & active
    '    If MyMerchant.ResponseCode = 0 And UCase(MyMerchant.Status) = "ACTIVE" And UCase(MyMerchant.MerchantType) = "INTERNAL" Then

    '        b4hashstring = "&&" + MyRequest.OTT_ID + "##" + MyMerchant.MerchantPWD + "&&"
    '        MySignature = MyOMGFunction.GetEncryption(b4hashstring)

    '        '2. Check matching signature
    '        If MySignature = MyRequest.Signature Then

    '            If MyRequest.SendType = 2 Then      'Send by SMS

    '            Else    'Send by Mail

    '                Dim SendMailReq As New INT_SendMailReq
    '                Dim SendMailResp As New INT_SendMailResp

    '                If MyRequest.Email Is Nothing Then
    '                    SendMailReq.Email = "haidah.hussaini@tm.com.my"
    '                    SendMailReq.Subject = "Unknown mail notification by OTT_ID = " & MyRequest.OTT_ID
    '                Else
    '                    SendMailReq.Email = MyRequest.Email
    '                    SendMailReq.Subject = MyRequest.EmailSubject
    '                End If

    '                SendMailReq.MsgBody = MyRequest.MessageBody.Replace("|", "<br>")
    '                SendMailResp = MyMailFunction.SendViaEmail(SendMailReq)

    '                If SendMailResp.ResponseMsg = "OK" Then
    '                    MyResponse.ResponseCode = 0
    '                    MyResponse.ResponseMsg = SendMailReq.MsgBody
    '                    'MyResponse.ResponseMsg = "Successful transaction"
    '                Else
    '                    MyResponse.ResponseCode = 108
    '                    MyResponse.ResponseMsg = "Failed transaction"
    '                    'ErrorMessage = "SendViaEmail: " & SendMailResp.ResponseMsg & "(" & SendMailResp.ResponseCode & ")"
    '                End If
    '            End If
    '        Else
    '            MyResponse.ResponseCode = 100
    '            MyResponse.ResponseMsg = "Signature does not match"
    '        End If
    '    Else
    '        Select Case MyMerchant.ResponseCode
    '            Case 0, 1       'INACTIVE & NOT EXIST
    '                MyResponse.ResponseCode = 102
    '                MyResponse.ResponseMsg = "Invalid OTT ID"
    '            Case 2
    '                MyResponse.ResponseCode = 101
    '                MyResponse.ResponseMsg = "Technical error"
    '                ErrorMessage = "M: " & MyMerchant.ResponseMsg
    '            Case Else
    '                MyResponse.ResponseCode = 101
    '                MyResponse.ResponseMsg = "Technical error"
    '                ErrorMessage = "M: " & MyMerchant.ResponseMsg & "(" & MyMerchant.ResponseCode & ")"
    '        End Select
    '    End If

    '    Return MyResponse
    'End Function

End Class
