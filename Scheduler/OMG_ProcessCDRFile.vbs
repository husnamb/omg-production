Call LogEntry()

Sub LogEntry()

        'Force the script to finish on an error.
        On Error Resume Next

        'Declare variables
        Dim objRequest
        Dim URL , strFromDate , strEndDate
		
		Set objRequest = CreateObject("Microsoft.XMLHTTP")

        'Put together the URL link appending the Variables.
        URL = "http://omg.hypp.tv/Scheduler/ProcessCDRFile.aspx"
        ' Development URL
'        URL = "http://omgdev.hypp.tv/Scheduler/ProcessCDRFile.aspx"

        'Open the HTTP request and pass the URL to the objRequest object
        objRequest.open "POST", URL , false

        'Send the HTML Request
        objRequest.Send

        'Set the object to nothing
        Set objRequest = Nothing

        If Err.Number <> 0 then
            Const cdoSendUsingPickup = 1
            Const cdoSendUsingPort = 2

            Const cdoAnonymous = 0
            Const cdoBasic = 1
            Const cdoNTLM = 2

            Set objMessage = CreateObject("CDO.Message") 
            objMessage.Subject = "OMG Process CDR Data"
            objMessage.From = "haidah.hussaini@tm.com.my"
            objMessage.To = "haidah.hussaini@tm.com.my" 

            textBody = "Error: " & Err.Number & vbCRLF & vbCRLF 
            textBody = textBody & "Error (Hex): " & Hex(Err.Number) & vbCRLF 
            textBody = textBody &  "Source: " &  Err.Source& vbCRLF 
            textBody = textBody & "Description: " &  Err.Description & vbCRLF

            objMessage.TextBody = textBody

            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 

            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.tm.com.my"

            'cdoAnonymous
            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoAnonymous

            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 

            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False

            objMessage.Configuration.Fields.Item _
            ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

            objMessage.Configuration.Fields.Update

            objMessage.Send

			Err.Clear
         End If

End Sub

